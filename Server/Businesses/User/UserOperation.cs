﻿using CommonLib.NetOperation;
using DispatchingCenter.Dispatch;
using Events.Auth;
using Events.User;
using Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Businesses.User
{
    /// <summary>
    /// 用户操作
    /// </summary>
    public class UserOperation
    {
        /// <summary>
        /// 查询所有分组的信息列表
        /// </summary>
        /// <returns></returns>
        public GroupList GetGroupList()
        {
            var getGroupListEvent = new GetGroupListEvent();

            Dispatcher.ActiveEvent(getGroupListEvent);

            return getGroupListEvent.GroupList;
        }

        /// <summary>
        /// 获取关注者列表
        /// </summary>
        /// <param name="userList"></param>
        /// <returns></returns>
        public UserList GetUserList(UserList userList)
        {
            var getUserListEvent = new GetUserListEvent();

            Dispatcher.ActiveEvent(getUserListEvent);

            return getUserListEvent.UserList;
        }

        /// <summary>
        /// 获取用户信息
        /// </summary>
        /// <param name="userInfoQueryCondition"></param>
        /// <returns></returns>
        public UserInfo GetUserInfo(UserInfoQueryCondition userInfoQueryCondition)
        {
            var getUserInfoEvent = new GetUserInfoEvent()
            {
                UserInfoQueryCondition = userInfoQueryCondition
            };
            Dispatcher.ActiveEvent(getUserInfoEvent);

            return getUserInfoEvent.UserInfo;
        }
    }
}

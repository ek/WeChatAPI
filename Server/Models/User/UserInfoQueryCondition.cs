﻿using Models.EnumData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.User
{
    /// <summary>
    /// 用户信息查询条件
    /// </summary>
    [DataContract(Name = "user_info_query_condition")]
    public class UserInfoQueryCondition
    {
        /// <summary>
        /// 普通用户的标识，对当前公众号唯一
        /// </summary>
        [DataMember(Name = "openid")]
        public string OpenID { get; set; }

        /// <summary>
        /// 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
        /// </summary>
        [DataMember(Name = "lang")]
        public string Language { get; set; }

        public Language LanguageEnum
        {
            get
            {
                Language language = EnumData.Language.zh_CN;
                Enum.TryParse(this.Language, out language);
                return language;
            }
        }
    }
}

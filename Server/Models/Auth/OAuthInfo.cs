﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Auth
{
    /// <summary>
    /// OAuth2.0条件
    /// </summary>
    public class OAuthCondition
    {
        /// <summary>
        /// 填写第一步获取的code参数
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// 授权类型
        /// </summary>
        public string GrantType { get { return "authorization_code"; } }
    }
}

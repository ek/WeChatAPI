﻿using CommonLib.NetOperation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Base
{
    public class BaseCommandRequest
    {
        public string URL { get; set; }

        public HttpMethod Method { get; set; }

        public object PostObject { get; set; }
    }
}

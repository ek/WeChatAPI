﻿using Models.Menu;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Base
{
    /// <summary>
    /// 菜单基类
    /// </summary>
    [DataContract(Name = "menu")]
    [KnownType(typeof(ClickMenu))]
    [KnownType(typeof(DropDownMenu))]
    [KnownType(typeof(ViewMenu))]
    public class BaseMenu
    {
        /// <summary>
        /// 菜单名称
        /// </summary>
        [DataMember(Name = "name")]
        public string Name { get; set; }
    }
}

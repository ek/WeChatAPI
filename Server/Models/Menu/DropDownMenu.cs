﻿using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.Menu
{
    /// <summary>
    /// 下拉菜单
    /// </summary>
    [DataContract(Name = "drop_down_menu")]
    public class DropDownMenu : BaseMenu
    {
        /// <summary>
        /// 子菜单
        /// </summary>
        [DataMember(Name = "sub_button")]
        public List<BaseMenu> MenuSet { get; set; }
    }
}

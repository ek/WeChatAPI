﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Models.Interface
{
    /// <summary>
    /// 微信接口交互凭据授权接口
    /// </summary>
    public interface IAccessTokenAuth
    {
        string AccessToken { get; set; }
    }
}

﻿using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;

namespace Models.SendMessage
{
    /// <summary>
    /// 客服文本消息
    /// </summary>
    [DataContract(Name = "customer_service_text_message")]
    [KnownType(typeof(BaseCustomerServiceMessage))]
    [KnownType(typeof(BaseTextMessage))]
    public class CustomerServiceTextMessage : BaseCustomerServiceMessage
    {
        /// <summary>
        /// 文本消息内容
        /// </summary>
        [DataMember(Name = "text")]
        public BaseTextMessage Content { get; set; }
    }
}

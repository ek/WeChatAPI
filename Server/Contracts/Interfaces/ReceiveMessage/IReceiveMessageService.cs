﻿using Models.SendMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WCFLib.ExceptionExtension;

namespace Contracts.Interfaces.ReceiveMessage
{
    [ServiceContract]
    public interface IReceiveMessageService
    {
        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        void ReceiveRawMessage(string rawMessage);
    }
}

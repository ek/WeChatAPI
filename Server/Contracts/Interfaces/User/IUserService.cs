﻿using Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceModel;
using System.Text;
using WCFLib.ExceptionExtension;

namespace Contracts.Interfaces.User
{
    [ServiceContract]
    public interface IUserService
    {
        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        UserList GetUserList(UserList userList);

        [OperationContract]
        [FaultContractAttribute(typeof(FaultMessage))]
        UserInfo GetUserInfo(UserInfoQueryCondition userInfoQueryCondition);
    }
}

﻿using DispatchingCenter.Base;
using Models.Auth;
using Models.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events.Auth
{
    /// <summary>
    /// 获取微信交互接口凭证事件
    /// </summary>
    public class GetAccessTokenEvent : DispatchEvent
    {
        /// <summary>
        /// 微信交互接口凭证信息
        /// </summary>
        public AccessTokenInfo AccessTokenInfo { get; set; }
    }
}

﻿using DispatchingCenter.Base;
using Models.User;
using Models.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Events.User
{
    /// <summary>
    /// 获取关注者列表
    /// </summary>
    public class GetUserListEvent : DispatchEvent, IAccessTokenAuth
    {
        /// <summary>
        /// 微信接口交互凭证
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// 关注者列表
        /// </summary>
        public UserList UserList { get; set; }
    }
}

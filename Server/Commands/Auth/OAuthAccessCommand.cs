﻿using Commands.Base;
using DispatchingCenter.Base;
using Events.Auth;
using Models.Auth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.Auth
{
    /// <summary>
    /// OAuth2.0授权访问命令
    /// </summary>
    public class OAuthAccessCommand
    {
        /// <summary>
        /// 获取OAuth2.0授权访问信息
        /// </summary>
        /// <param name="e"></param>
        [DispatchHandler(typeof(GetOAuthAccessInfoEvent))]
        public void GetOAuthAccessInfo(GetOAuthAccessInfoEvent e)
        {
            var commandRequest = new GetOAuthAccessInfoCommandRequest(e.OAuthCondition);
            e.OAuthAccessInfo = CommandHelper.GetWeChatResponseObject<OAuthAccessInfo>(commandRequest);
        }
    }
}

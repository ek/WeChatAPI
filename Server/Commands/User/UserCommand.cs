﻿using Commands.Base;
using DispatchingCenter.Base;
using Events.User;
using Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.User
{
    /// <summary>
    /// 用户命令
    /// </summary>
    public class UserCommand
    {
        /// <summary>
        /// 获取关注者列表
        /// </summary>
        /// <param name="e"></param>
        [DispatchHandler(typeof(GetUserListEvent))]
        public void GetUserList(GetUserListEvent e)
        {
            var commandRequest = new GetUserListCommandRequest(e.AccessToken, e.UserList == null ? "" : e.UserList.NextOpenID);

            e.UserList = CommandHelper.GetWeChatResponseObject<UserList>(commandRequest);
        }

        /// <summary>
        /// 获取用户基本信息
        /// </summary>
        /// <param name="e"></param>
        [DispatchHandler(typeof(GetUserInfoEvent))]
        public void GetUserInfo(GetUserInfoEvent e)
        {
            var commandRequest = new GetUserInfoCommandRequest(e.AccessToken, e.UserInfoQueryCondition.OpenID, e.UserInfoQueryCondition.LanguageEnum);

            e.UserInfo = CommandHelper.GetWeChatResponseObject<UserInfo>(commandRequest);
        }
    }
}

﻿using CommonLib.NetOperation;
using Models.Base;
using Models.EnumData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.User
{
    /// <summary>
    /// 获取用户列表指令请求
    /// </summary>
    public class GetUserListCommandRequest : BaseCommandRequest
    {
        public GetUserListCommandRequest(string accessToken, string nextOpenID)
        {
            this.Method = HttpMethod.GET;
            this.URL = string.Format("https://api.weixin.qq.com/cgi-bin/user/get?access_token={0}&next_openid={1}",
                accessToken,
                nextOpenID);
        }
    }

    /// <summary>
    /// 获取用户信息指令请求
    /// </summary>
    public class GetUserInfoCommandRequest : BaseCommandRequest
    {
        public GetUserInfoCommandRequest(string accessToken, string openID, Language language)
        {
            this.Method = HttpMethod.GET;
            this.URL = string.Format("https://api.weixin.qq.com/cgi-bin/user/info?access_token={0}&openid={1}&lang={2}",
                accessToken,
                openID,
                language);
        }
    }
}

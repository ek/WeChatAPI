﻿using Commands.Auth;
using Commands.Base;
using CommonLib.CacheOperation;
using DispatchingCenter.Base;
using Events.SendMessage;
using Models.Data;
using Models.User;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.Menu
{
    /// <summary>
    /// 菜单命令
    /// </summary>
    public class MenuCommand
    {
        /// <summary>
        /// 创建菜单
        /// </summary>
        /// <param name="e"></param>
        [DispatchHandler(typeof(CreateMenuEvent))]
        public void CreateMenu(CreateMenuEvent e)
        {
            var commandRequest = new CreateMenuCommandRequest(e.AccessToken, e.MenuList);

            CommandHelper.NoReturnWeChatRequest(commandRequest);
        }

        /// <summary>
        /// 查询菜单
        /// </summary>
        /// <param name="e"></param>
        [DispatchHandler(typeof(GetMenuEvent))]
        public void GetMenu(GetMenuEvent e)
        {
            var commandRequest = new GetMenuCommandRequest(e.AccessToken);

            e.MenuList = CommandHelper.GetWeChatResponseObject<Models.Menu.MenuList>(commandRequest);
        }
    }
}

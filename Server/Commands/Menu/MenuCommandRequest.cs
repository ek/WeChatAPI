﻿using CommonLib.CacheOperation;
using CommonLib.NetOperation;
using Models.Base;
using Models.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.Menu
{
    /// <summary>
    /// 创建菜单命令请求
    /// </summary>
    public class CreateMenuCommandRequest : BaseCommandRequest
    {
        public CreateMenuCommandRequest(string accessToken, object postObject)
        {
            this.Method = HttpMethod.POST;
            this.URL = string.Format("https://api.weixin.qq.com/cgi-bin/menu/create?access_token={0}", accessToken);
            this.PostObject = postObject;
        }
    }

    /// <summary>
    /// 查询菜单命令请求
    /// </summary>
    public class GetMenuCommandRequest : BaseCommandRequest
    {
        public GetMenuCommandRequest(string accessToken)
        {
            this.Method = HttpMethod.GET;
            this.URL = string.Format("https://api.weixin.qq.com/cgi-bin/menu/get?access_token={0}", accessToken);
        }
    }
}

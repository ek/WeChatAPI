﻿using CommonLib.CacheOperation;
using CommonLib.NetOperation;
using Models.Base;
using Models.Data;
using Models.SendMessage;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Commands.SendMessage
{
    /// <summary>
    /// 发送客服消息命令请求
    /// </summary>
    public class SendCustomerServiceMessageCommandRequest : BaseCommandRequest
    {
        public SendCustomerServiceMessageCommandRequest(string accessToken, BaseCustomerServiceMessage customerServiceMessage)
        {
            this.Method = HttpMethod.POST;
            this.URL = string.Format("https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token={0}", accessToken);
            this.PostObject = customerServiceMessage;
        }
    }
}

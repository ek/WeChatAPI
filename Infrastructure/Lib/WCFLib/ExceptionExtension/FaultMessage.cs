﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using WCFLib.ExceptionExtension.Enums;

namespace WCFLib.ExceptionExtension
{
    /// <summary>
    /// 错误信息实体类（用于错误契约FaultContract）
    /// </summary>
    [DataContract]
    [KnownType(typeof(ExceptionType))]
    public class FaultMessage
    {
        /// <summary>
        /// 错误信息
        /// </summary>
        [DataMember]
        public string Message { get; set; }

        /// <summary>
        /// 错误代码
        /// </summary>
        [DataMember]
        public int ErrorCode { get; set; }

        /// <summary>
        /// 异常类型
        /// </summary>
        [DataMember]
        public ExceptionType Type { get; set; }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Reflection;
using System.ServiceModel.Configuration;
using System.Text;

namespace WCFLib.Service
{
    /// <summary>
    /// 服务工具类
    /// </summary>
    public class ServiceHelper
    {
        /// <summary>
        /// 获取服务类型
        /// </summary>
        /// <param name="asm">包含服务的程序集</param>
        /// <returns></returns>
        public static List<Type> GetServiceType(Assembly asm)
        {
            var config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            var sectionGroups = config.SectionGroups["system.serviceModel"];
            var serviceModelSectionGroup = sectionGroups as ServiceModelSectionGroup;
            var services = serviceModelSectionGroup.Services.Services.Cast<ServiceElement>();

            List<Type> lstType = new List<Type>();

            foreach (var service in services)
            {
                var serviceType = asm.GetType(service.Name);

                if (serviceType == null)
                {
                    throw new TypeLoadException("不存在的类型:" + service.Name);
                }

                lstType.Add(serviceType);
            }

            return lstType;
        }
    }
}

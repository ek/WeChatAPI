﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace WeChatService.Test
{
    [TestClass]
    public class MenuTest
    {
        [TestMethod]
        public void CreateMenu()
        {
            var client = new WeChat.MenuServiceClient();
            client.CreateMenu(new WeChat.menu_list()
            {
                button = new System.Collections.Generic.List<WeChat.menu>()
                {
                    new WeChat.drop_down_menu()
                    {
                        name = "测试官网",
                        sub_button = new System.Collections.Generic.List<WeChat.menu>()
                        {
                            new WeChat.view_menu()
                            {
                                name = "官网地址",
                                url = "http://www.xxx.com"
                            },
                        }
                    }
                }
            });
        }

        [TestMethod]
        public void GetMenu()
        {
            var client = new WeChat.MenuServiceClient();
            var menus = client.GetMenu();
        }
    }
}
